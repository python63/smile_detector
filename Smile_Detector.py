
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import numpy as np
import cv2 as cv
import argparse
import imutils


vArgPar = argparse.ArgumentParser()
vArgPar.add_argument("-m","--model",required=True,help="Path to the pre-trained model")
vArgPar.add_argument("-c","--cascade",required=True,help="path to where the face cascade resides")
vArgPar.add_argument("-i","--image",required=True,help="Path to the image")
args = vars(vArgPar.parse_args())


def main():

    #Loading model and cascade detector
    
    v_Model = load_model(args["model"])
    v_Detector = cv.CascadeClassifier(args["cascade"])
    v_Image = cv.imread(args["image"])


    #Converting to grey
    v_GreyImage = cv.cvtColor(v_Image,cv.COLOR_BGR2GRAY)
    v_Rect = v_Detector.detectMultiScale(v_GreyImage,scaleFactor=1.1,minNeighbors=5,minSize=(30,30),flags=cv.CASCADE_SCALE_IMAGE)


    for (X_start,Y_start,height,width,) in v_Rect:
        cv.rectangle(v_Image,(X_start,Y_start),(X_start+width,Y_start+height),(255,0,0),2)

        v_SubFace = v_GreyImage[Y_start:(Y_start+height),X_start:(X_start+width)]
        v_SubFace = imutils.resize(v_SubFace,width=28,height=28)
        v_SubFace = v_SubFace.astype("float") / 255.0
        v_SubFace = img_to_array(v_SubFace)
        v_SubFace = np.expand_dims(v_SubFace,axis=0)

        (not_Smiling,Smiling) = v_Model.predict(v_SubFace)[0]
        print(not_Smiling,Smiling)

        v_Label = "Smiling" if Smiling > not_Smiling else "Not Smiling"
        cv.putText(v_Image,v_Label,(X_start,Y_start - 10),cv.FONT_HERSHEY_COMPLEX,0.5,(0,0,255),1)





    
    cv.imshow('Name',v_Image)
    cv.waitKey(0)




if __name__ == "__main__":
    main()
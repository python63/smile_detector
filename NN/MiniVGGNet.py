
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dropout
from tensorflow.keras import backend as K


class MiniVGGNet:
    @staticmethod
    def build(width,height,depth,classes):

        model = Sequential()
        

        #index of the channel dimension, for Batch normalization. It operates over the channel
        #we need to know which varaible we need to optimize, channel is last by defalut, so index -1 indicates on last position
        channel_dimension = -1 
        vInput_size = (width,height,depth)

        #But, if we are using channel_first, channel is first
        if K.image_data_format() == "channel first":
            vInput_size = (depth,width,height)
            channel_dimension = 1

        #Constructing the model

        model.add(Conv2D(32,(3,3),input_shape=vInput_size,padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=channel_dimension))
        model.add(Conv2D(32,(3,3),padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=channel_dimension))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.25))
        model.add(Conv2D(64,(3,3),padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=channel_dimension))
        model.add(Conv2D(64,(3,3),padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=channel_dimension))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.25))
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=channel_dimension))
        model.add(Dropout(0.5))
        model.add(Dense(classes))
        model.add(Activation("softmax"))

        return model

        




#Smile detector using CNN and Computer Vision Tasks

#Adding module from another folder
import sys
sys.path.insert(1,'/home/myubuntu/Desktop/DLCV/NN/CONV/')
from shallownet import ShallowNet

#Basic modules:
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.python.keras.utils import np_utils
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import cv2 as cv
import os
import imutils

#Creating the arguments, that we need to pass to our program
# --dataset - path to the SMILE directory on a disk
# --model - path, where the serialized model(based on the LeNet architecture) will be saved
vArgPar = argparse.ArgumentParser()
vArgPar.add_argument("-d","--dataset",required=True,help="Path to the datset")
vArgPar.add_argument("-m","--model",required=True,help="Path to the output model")
args = vars(vArgPar.parse_args())

def main():
    #Initialization of the list of the data and labels
    v_Data = []
    v_Labels = []


    #Converting data to the 
    for t_ImagePath in sorted(list(paths.list_images(args["dataset"]))):

        t_Image = cv.imread(t_ImagePath)
        t_Image = cv.cvtColor(t_Image,cv.COLOR_BGR2GRAY)
        t_Image = imutils.resize(t_Image,width=28,height=28)
        t_Image = img_to_array(t_Image)
        v_Data.append(t_Image)

        #Extracting the label of the image:
        t_label = t_ImagePath.split(os.path.sep)[-3]
        t_label = "smiling" if t_label == "positives" else "not_smiling"
        v_Labels.append(t_label)

    v_Data = np.array(v_Data,dtype="float") / 255.0
    v_Labels = np.array(v_Labels)

    v_LabEnc = LabelEncoder().fit(v_Labels)
    v_Labels = np_utils.to_categorical(v_LabEnc.transform(v_Labels),2)


    v_ClassTotals = v_Labels.sum(axis=0)
    v_ClassWeights = v_ClassTotals.max() / v_ClassTotals
    print(v_ClassWeights)

    #Diiding data into sets:
    (TrainX,TestX,TrainY,TestY) = train_test_split(v_Data,v_Labels,test_size=0.2,stratify=v_Labels)

    v_Model = ShallowNet.build(width=28,height=28,depth=1,classes=2)
    v_Model.compile(loss="binary_crossentropy",optimizer="adam",metrics=["accuracy"])
    v_Epochs = 30

    Res = v_Model.fit(TrainX,TrainY,validation_data=(TestX,TestY),class_weight=v_ClassWeights,batch_size=32,epochs=v_Epochs,verbose=1)

    #Evaluation of the network
    v_Predictions = v_Model.predict(TestX,batch_size=32)
    print(classification_report(TestY.argmax(axis=1),v_Predictions.argmax(axis=1),target_names=v_LabEnc.classes_))

    #Model serialization
    v_Model.save(args["model"])

    plt.style.use("ggplot")
    plt.figure()
    plt.plot(np.arange(0, v_Epochs), Res.history["loss"], label="train_loss")
    plt.plot(np.arange(0, v_Epochs), Res.history["val_loss"], label="val_loss")
    plt.plot(np.arange(0, v_Epochs), Res.history["acc"], label="acc")
    plt.plot(np.arange(0, v_Epochs), Res.history["val_acc"], label="val_acc")
    plt.title("Training Loss and Accuracy")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
    


